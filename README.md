# Kindle Collection

A Group of utilities for Kindle

Trying to get the most out of Kindle eReader, you start thinking about the missing pieces that will streamline your reading experience.
I was missing couple of features so I created this Repo.

## 1. Adding books to Kindle
Amazon have a service where you can register an email address as source and another as a destination and it allows you to send books directly to Kindle.
this is quite useful, however it is not streamlined.

What I have done is, I took the free tier Google Apps Scripts and automated the flow that:
- whenever a book is placed inside a folder inside Google drive
- It will trigger an email to Kindle address
- move the file to a new folder to avoid duplicates.

-> The script is Send2kindle.js


## 2. Adding Pocket articles to Kindle
Sometimes there are articles that are interesting but is considered a long read.
For those, I wanted to be able to read them on my kindle.

- I use pocket for managing my articles
- with IFTT.com i create a new service ( https://ifttt.com/applets/EfB7ZSG2 ), the service will sense new articles tagged with Kindle and instiate an email from my gmail.
- the email will contain only the url, I need to extract the actual article.
- for that I use ukeeper.com, after registering the from to be my gmail and destination to be kindle address.
- for it work, we need to shutdown url shortening in Pocket.
