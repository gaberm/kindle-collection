function processKindleFiles() {

    try {

        var folderId = "0BwgWw6nnA_fCQW5XQzhtUU00TjQ";
        // Get folder by id
        var parentFolder = DriveApp.getFolderById(folderId);
        var files = parentFolder.getFiles();
        while (files.hasNext()) {
            var childFile = files.next();
            sendToKindle(childFile);
            //Once Attached, Move the file to the root of the folder
            var destFolderId = "0BwgWw6nnA_fCeFJEYlhlSUFsbzg";
            var source_folder = DriveApp.getFolderById(folderId);
            var dest_folder = DriveApp.getFolderById(destFolderId);
            dest_folder.addFile(childFile);
            source_folder.removeFile(childFile);

        }

    } catch (e) {

        Logger.log(e.toString());
    }

};


function sendToKindle(driveFile) {

    var fileToAttach = driveFile.getBlob();
    var subject = "convert";
    var body = driveFile.getName();
    var email = "<myKindleMailId>@kindle.com";
    if (MailApp.getRemainingDailyQuota() > 0)
        GmailApp.sendEmail(email, subject, body, {
            htmlBody: body,
            attachments: [fileToAttach]
        });

    Logger.log("File sent to Kindle");

}



function sendDailyNewsToKindle() {
  
var todayDate=Utilities.formatDate(new Date(), Session.getScriptTimeZone(), "dd.MM.YY");
var url="http://anynewsite.com/{dt}";
var urlPick=url.replace("{dt}",todayDate);
var response = UrlFetchApp.fetch(urlPick , {});
    
var todayDateN=Utilities.formatDate(new Date(), Session.getScriptTimeZone(), "dd.MMM.YYYY");
//convert the response to a blob and store in our array
var file1 = response.getBlob().setName(todayDateN + "News.pdf");

var subject="convert";
var body="Murli Content";
var email="<uniqueKindleMailId>@kindle.com";
if (MailApp.getRemainingDailyQuota() > 0) 
GmailApp.sendEmail(email, subject, body, {
  htmlBody: body,
  attachments:[file1]     
});  

  
}


